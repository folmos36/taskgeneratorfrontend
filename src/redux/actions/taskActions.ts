import { ActionCreator, Dispatch } from 'redux';
import { ThunkAction } from 'redux-thunk';

import { TaskActionEnum } from '../enums/TaskActionEnum';
import { ITask } from '../interfaces/ITask';
import { IListTaskLoaded, ILoadingListTask } from '../interfaces/ITaskActionTypes';
import { ITaskState } from '../interfaces/ITaskState';

export const loadingTaskList: ActionCreator<
	ThunkAction<void, ITaskState, unknown, ILoadingListTask>
> = () => {
	return (dispatch: Dispatch<ILoadingListTask>) => {
		const loadingTaskList: ILoadingListTask = {
			type: TaskActionEnum.LOADING_LIST,
			payload: {
				loading: true,
				tasks: []
			}
		};
		try {
			dispatch(loadingTaskList);
		} catch (error) {
			loadingTaskList.payload = Object.assign({}, loadingTaskList.payload, {
				error: error.message
			});
			dispatch(loadingTaskList);
		}
	};
};

export const loadedTaskList: ActionCreator<
	ThunkAction<void, ITaskState, unknown, IListTaskLoaded>
> = () => {
	return async (dispatch: Dispatch<IListTaskLoaded>) => {
		const tasksListLoaded: IListTaskLoaded = {
			type: TaskActionEnum.LIST_LOADED,
			payload: {
				loading: false,
				tasks: []
			}
		};
		try {
			const response = await fetch(process.env.TASK_ENDPOINT as string);
			if (!response.ok) {
				const message = `An error has occured: ${response.status}`;
				throw new Error(message);
			}
			const tasks: ITask[] = await response.json();
			tasksListLoaded.payload = Object.assign({}, tasksListLoaded.payload, {
				tasks
			});
			dispatch(tasksListLoaded);
		} catch (error) {
			tasksListLoaded.payload = Object.assign({}, tasksListLoaded.payload, {
				error: error.message
			});
			dispatch(tasksListLoaded);
		}
	};
};
