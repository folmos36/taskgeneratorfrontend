import { ActionCreator, Dispatch } from 'redux';
import { ThunkAction } from 'redux-thunk';

import { SingleTaskActionEnum } from '../enums/SingleTaskActionEnum';
import { IMarkedTask, IMarkingTask, IResetMarkTask } from '../interfaces/ISingleTaskActionType';
import { ISingleTaskState } from '../interfaces/ISingleTaskState';

export const markingTask: ActionCreator<
	ThunkAction<void, ISingleTaskState, unknown, IMarkingTask>
> = (id: string) => {
	return async (dispatch: Dispatch<IMarkingTask>) => {
		const markingTask: IMarkingTask = {
			type: SingleTaskActionEnum.MARKING_TASK,
			payload: {
				loading: true,
				id,
				ok: false
			}
		};
		try {
			dispatch(markingTask);
		} catch (error) {
			markingTask.payload = Object.assign({}, markingTask.payload, {
				error: error.message
			});
			dispatch(markingTask);
		}
	};
};

export const resetMarkTask: ActionCreator<
	ThunkAction<void, ISingleTaskState, unknown, IResetMarkTask>
> = () => {
	return async (dispatch: Dispatch<IResetMarkTask>) => {
		const reset: IResetMarkTask = {
			type: SingleTaskActionEnum.RESET,
			payload: {
				error: "",
				loading: false,
				id: "",
				ok: false
			}
		};
		try {
			dispatch(reset);
		} catch (error) {
			reset.payload = Object.assign({}, reset.payload, {
				error: error.message
			});
			dispatch(reset);
		}
	};
};

export const markedTask: ActionCreator<
	ThunkAction<void, ISingleTaskState, unknown, IMarkedTask>
> = (id: string) => {
	return async (dispatch: Dispatch<IMarkedTask>) => {
		const markedTask: IMarkedTask = {
			type: SingleTaskActionEnum.MARKED_TASK,
			payload: {
				loading: false,
				id,
				ok: true
			}
		};
		try {
			const response = await fetch(process.env.TASK_ENDPOINT as string, {
				method: "PUT",
				headers: {
					"Content-Type": "application/json"
				},
				body: JSON.stringify({ id })
			});
			if (!response.ok) {
				const message = `An error has occured: ${response.status}`;
				throw new Error(message);
			}
			dispatch(markedTask);
		} catch (error) {
			markedTask.payload = Object.assign({}, markedTask.payload, {
				error: error.message,
				ok: false
			});
			dispatch(markedTask);
		}
	};
};
