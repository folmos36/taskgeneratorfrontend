import { Action } from 'redux';

import { TaskActionEnum } from '../enums/TaskActionEnum';
import { IPayLoadTask } from './IPayloadTask';

export interface ILoadingListTask extends Action<TaskActionEnum.LOADING_LIST> {
	payload: IPayLoadTask;
}

export interface IListTaskLoaded extends Action<TaskActionEnum.LIST_LOADED> {
	payload: IPayLoadTask;
}
