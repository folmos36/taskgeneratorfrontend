import { Action } from 'redux';

import { SingleTaskActionEnum } from '../enums/SingleTaskActionEnum';
import { ISinglePayLoadTask } from './ISinglePayLoadTask';

export interface IMarkingTask
	extends Action<SingleTaskActionEnum.MARKING_TASK> {
	payload: ISinglePayLoadTask;
}
export interface IMarkedTask extends Action<SingleTaskActionEnum.MARKED_TASK> {
	payload: ISinglePayLoadTask;
}

export interface IResetMarkTask extends Action<SingleTaskActionEnum.RESET> {
	payload: ISinglePayLoadTask;
}
