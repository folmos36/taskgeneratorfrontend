import { ITask } from './ITask';

export interface IPayLoadTask {
	error?: string;
	loading: boolean;
	tasks: ITask[];
}
