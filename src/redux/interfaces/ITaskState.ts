import { ITask } from './ITask';

export interface ITaskState {
	error?: string;
	loading: boolean;
	tasks: ITask[];
}
