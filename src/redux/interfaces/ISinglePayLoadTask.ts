export interface ISinglePayLoadTask {
	error?: string;
	loading: boolean;
	id: string;
	ok: boolean;
}
