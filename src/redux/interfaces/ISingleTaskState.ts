export interface ISingleTaskState {
	error?: string;
	loading: boolean;
	id: string;
	ok: boolean;
}
