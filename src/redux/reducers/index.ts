import { combineReducers } from 'redux';

import { SingleTaskReducer } from './SingleTaskReducer';
import { TaskReducer } from './TaskReducer';

export const rootReducer = combineReducers({
	task: TaskReducer,
	single: SingleTaskReducer
});
