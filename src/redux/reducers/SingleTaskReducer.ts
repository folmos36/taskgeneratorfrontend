import { SingleTaskActionEnum } from '../enums/SingleTaskActionEnum';
import { ISinglePayLoadTask } from '../interfaces/ISinglePayLoadTask';
import { IMarkedTask, IMarkingTask, IResetMarkTask } from '../interfaces/ISingleTaskActionType';
import { ISingleTaskState } from '../interfaces/ISingleTaskState';

const initialState: ISingleTaskState = {
	loading: false,
	id: "",
	ok: false
};

export function SingleTaskReducer(
	state: ISingleTaskState = initialState,
	action: IMarkingTask | IMarkedTask | IResetMarkTask
): ISinglePayLoadTask {
	switch (action.type) {
		case SingleTaskActionEnum.MARKING_TASK:
			return Object.assign({}, state, action.payload);
		case SingleTaskActionEnum.MARKED_TASK:
			return Object.assign({}, state, action.payload);
		case SingleTaskActionEnum.RESET:
			return Object.assign({}, state, action.payload);
		default:
			return state;
	}
}
