import { TaskActionEnum } from '../enums/TaskActionEnum';
import { IPayLoadTask } from '../interfaces/IPayloadTask';
import { IListTaskLoaded, ILoadingListTask } from '../interfaces/ITaskActionTypes';
import { ITaskState } from '../interfaces/ITaskState';

const initialState: ITaskState = {
	loading: false,
	tasks: []
};

export function TaskReducer(
	state: ITaskState = initialState,
	action: ILoadingListTask | IListTaskLoaded
): IPayLoadTask {
	switch (action.type) {
		case TaskActionEnum.LOADING_LIST:
			return Object.assign({}, state, action.payload);
		case TaskActionEnum.LIST_LOADED:
			return Object.assign({}, state, action.payload);
		default:
			return state;
	}
}
