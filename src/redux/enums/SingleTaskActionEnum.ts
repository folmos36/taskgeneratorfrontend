export enum SingleTaskActionEnum {
	MARKING_TASK = "MARKING_TASK",
	MARKED_TASK = "MARKED_TASK",
	RESET = "RESET"
}
