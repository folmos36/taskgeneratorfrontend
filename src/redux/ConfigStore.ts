import { applyMiddleware, createStore } from 'redux';
import { composeWithDevTools } from 'redux-devtools-extension';
import reduxImmutableStateInvariant from 'redux-immutable-state-invariant';
import thunk from 'redux-thunk';

import { rootReducer } from './reducers';

export default function configureStore(initialState: any) {
	return createStore(
		rootReducer,
		initialState,
		composeWithDevTools(applyMiddleware(reduxImmutableStateInvariant(), thunk))
	);
}
