import clsx from 'clsx';
import React, { useEffect } from 'react';
import ReactDOM from 'react-dom';
import { TypedUseSelectorHook, useSelector as useReduxSelector } from 'react-redux';
import { createSelector } from 'reselect';

import {
    AppBar, Button, Card, CardActions, CardContent, CircularProgress, CssBaseline, Dialog,
    DialogActions, DialogContent, DialogContentText, DialogTitle, Toolbar, Typography
} from '@material-ui/core';
import { createStyles, makeStyles, Theme } from '@material-ui/core/styles';

import { useActions } from '../redux/actions/bindActionsCreators';
import { markedTask, markingTask, resetMarkTask } from '../redux/actions/singleTaskActions';
import { loadedTaskList, loadingTaskList } from '../redux/actions/taskActions';
import { ITask } from '../redux/interfaces/ITask';
import { rootReducer } from '../redux/reducers';

type modelProps = {
	handleCloseDialog: () => void;
	openDialog: boolean;
	loadingToCompleteTask: any;
	completeTask: any;
	task: ITask;
};
type RootState = ReturnType<typeof rootReducer>;
const useSelector: TypedUseSelectorHook<RootState> = useReduxSelector;
const getTaskSelector = createSelector(
	(state: RootState) => state.task,
	(task) => task
);

const getSingleTaskSelector = createSelector(
	(state: RootState) => state.single,
	(single) => single
);

const drawerWidth = 0;

const useStyles = makeStyles((theme: Theme) =>
	createStyles({
		root: {
			display: "flex"
		},
		appBar: {
			transition: theme.transitions.create(["margin", "width"], {
				easing: theme.transitions.easing.sharp,
				duration: theme.transitions.duration.leavingScreen
			})
		},
		appBarShift: {
			width: `calc(100% - ${drawerWidth}px)`,
			transition: theme.transitions.create(["margin", "width"], {
				easing: theme.transitions.easing.easeOut,
				duration: theme.transitions.duration.enteringScreen
			}),
			marginRight: drawerWidth
		},
		title: {
			flexGrow: 1
		},

		drawerHeader: {
			display: "flex",
			alignItems: "center",
			padding: theme.spacing(0, 1),
			// necessary for content to be below app bar
			...theme.mixins.toolbar,
			justifyContent: "flex-start"
		},
		content: {
			flexGrow: 1,
			padding: theme.spacing(3),
			transition: theme.transitions.create("margin", {
				easing: theme.transitions.easing.sharp,
				duration: theme.transitions.duration.leavingScreen
			}),
			marginRight: -drawerWidth
		},
		contentShift: {
			transition: theme.transitions.create("margin", {
				easing: theme.transitions.easing.easeOut,
				duration: theme.transitions.duration.enteringScreen
			}),
			marginRight: 0
		},
		cardRoot: {
			width: 385
		},

		cardTitle: {
			fontSize: 14
		},
		pos: {
			marginTop: 12,
			marginBottom: 12
		},
		container: {
			display: "flex",
			flexWrap: "wrap",
			"& > *": {
				margin: theme.spacing(1)
			}
		}
	})
);
const Modal = ({
	openDialog,
	handleCloseDialog,
	loadingToCompleteTask,
	completeTask,
	task
}: modelProps) => {
	const singlePayLoad = useSelector(getSingleTaskSelector);
	const loading = singlePayLoad.loading ? (
		<CircularProgress />
	) : singlePayLoad.ok ? (
		"Completed"
	) : (
		<React.Fragment></React.Fragment>
	);
	if (singlePayLoad.loading) {
		completeTask(task.id);
	}
	const error = singlePayLoad.error ? (
		singlePayLoad.error
	) : (
		<React.Fragment></React.Fragment>
	);
	return ReactDOM.createPortal(
		<Dialog
			open={openDialog}
			onClose={() => {
				handleCloseDialog();
			}}
			aria-labelledby="alert-dialog-title"
			aria-describedby="alert-dialog-description"
		>
			<DialogTitle id="alert-dialog-title">{`Task # ${task.id}`}</DialogTitle>
			<DialogContent>
				<DialogContentText id="alert-dialog-description">
					{task.name}
				</DialogContentText>
				<br />
				{loading}
				{error}
			</DialogContent>
			<DialogActions>
				<Button onClick={handleCloseDialog} color="primary">
					Cancel
				</Button>
				<Button
					onClick={() => loadingToCompleteTask(task.id)}
					color="primary"
					autoFocus
				>
					Mark as completed
				</Button>
			</DialogActions>
		</Dialog>,
		document.body
	);
};
export const App = () => {
	const [open, setOpen] = React.useState(false);
	const [task, setTask] = React.useState<ITask>({ id: "", name: "" });
	const classes = useStyles();
	const taskPayLoad = useSelector(getTaskSelector);
	const [
		loadingList,
		loadedList,
		loadingToCompleteTask,
		completeTask,
		resetCompleteAction
	] = useActions([
		loadingTaskList,
		loadedTaskList,
		markingTask,
		markedTask,
		resetMarkTask
	]);

	const handleClickOpen = (task: ITask) => {
		setOpen(true);
		setTask(task);
	};

	const handleClose = () => {
		setOpen(false);
		resetCompleteAction();
	};
	useEffect(() => loadingList(), []);

	if (taskPayLoad.loading) {
		loadedList();
	}
	return (
		<div className={classes.root}>
			<CssBaseline />
			<AppBar
				position="fixed"
				className={clsx(classes.appBar, {
					[classes.appBarShift]: open
				})}
			>
				<Toolbar>
					<Typography variant="h6" noWrap className={classes.title}>
						Task Manager
					</Typography>
				</Toolbar>
			</AppBar>
			<main
				className={clsx(classes.content, {
					[classes.contentShift]: open
				})}
			>
				<div className={classes.drawerHeader} />
				<div className={classes.container}>
					{taskPayLoad.tasks.map((task) => (
						<Card key={task.id} className={classes.cardRoot} variant="outlined">
							<CardContent>
								<Typography className={classes.pos} color="textSecondary">
									{task.id}
								</Typography>
								<Typography variant="body2" component="p">
									{task.name}
								</Typography>
							</CardContent>
							<CardActions>
								<Button size="small" onClick={() => handleClickOpen(task)}>
									Action
								</Button>
							</CardActions>
						</Card>
					))}
				</div>
				<Modal
					handleCloseDialog={handleClose}
					openDialog={open}
					task={task}
					loadingToCompleteTask={loadingToCompleteTask}
					completeTask={completeTask}
				/>
			</main>
		</div>
	);
};
