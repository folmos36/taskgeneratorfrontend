const path = require("path");
const HtmlWebpackPlugin = require("html-webpack-plugin");
const Dotenv = require("dotenv-webpack");
module.exports = {
	mode: "production",
	entry: "./src/index",
	output: {
		path: path.resolve(__dirname, "dist"),
		filename: "[name].js",
		chunkFilename: "[name].js",
		publicPath: "/"
	},
	resolve: {
		extensions: [".ts", ".tsx", ".js"]
	},
	devServer: {
		historyApiFallback: true
	},
	module: {
		rules: [
			{
				test: /\.ts(x?)$/,
				exclude: /node_modules/,
				use: [
					{
						loader: "ts-loader"
					}
				]
			},
			{
				test: /\.js$/,
				exclude: /node_modules/,
				use: {
					loader: "babel-loader"
				}
			},
			{
				test: /\.css$/,
				use: ["style-loader", "css-loader"]
			},
			{
				test: /\.(eot|svg|ttf|woff|woff2|otf)$/,
				use: [
					{
						loader: "file-loader",
						options: {
							name: "[name].[ext]",
							limit: 10000,
							mimetype: "application/font-woff"
						}
					}
				]
			},
			{
				test: /\.(jpg|png)$/,
				use: {
					loader: "url-loader"
				}
			}
		]
	},
	plugins: [
		new Dotenv({
			path: "./.env.production"
		}),
		new HtmlWebpackPlugin({
			template: "src/index.html",
			favicon: "src/images/favicon.ico"
		})
	],
	optimization: {
		runtimeChunk: {
			name: "manifest"
		},
		splitChunks: {
			cacheGroups: {
				vendor: {
					test: /[\\/]node_modules[\\/]/,
					name: "vendors",
					priority: -20,
					chunks: "all"
				}
			}
		}
	}
};
